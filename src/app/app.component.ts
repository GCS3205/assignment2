import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { RegisterPage } from '../pages/register/register';
import { ContactPage } from '../pages/contact/contact';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  username:any = "John";
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
    //console.log(this.listItem);

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  goAbout(){
    //console.log("About");
    this.nav.push(AboutPage,{"val":"This is from menu"});
  }
  goContact(){
    //console.log("About");
    this.nav.push(ContactPage,{"val":"This is from menu"});
  }
  goHome(){
    //console.log("Home");
    this.nav.setRoot(HomePage);
  }
}
