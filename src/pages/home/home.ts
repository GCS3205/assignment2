import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  heading:any = "Card Heading";
  constructor(public navCtrl: NavController) {
    //console.log(this.heading);
  }

}
